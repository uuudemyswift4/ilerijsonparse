//
//  TwitterModel.swift
//  TwitterParseSwift4
//
//  Created by Kerim Çağlar on 07/07/2017.
//  Copyright © 2017 Kerim Çağlar. All rights reserved.
//

import Foundation

struct TwitterModel:Codable{
    
    var id:Int
    var text:String
    var user:User
    
    struct User:Codable{
        var id_str:String
        var name:String
    }
}
